#!/usr/env/bin python

class Perro():
    def __init__(self, nombre):
        self.nombre = nombre
        self.horas = 0
        self.set_tomar_aguita()
        self.get_hora_toma_agua()
        self.caminar()
    
    def set_tomar_aguita(self):
        self.horas = (self.horas + 1) % 6
        if self.horas == 5 or self.horas == 0:
            print("El perro firulais debe tomar agua ")
        else: 
            print("EL perro firulais no puede beber agua ")
            print(f'Bebio agua hace {self.horas} horas ')

    def get_hora_toma_agua(self):
        return self.horas
    
    def caminar(self):
        if self.horas == 5:
            print("El perro firulais puede salir a caminar")
        elif self.horas <= 4: 
            print("El perro firulais no puede salir a caminar")
            

valor = 3
print("----------------------------------------")
firulais = Perro("ComoTu")
print("----------------------------------------")

while valor != 0:
    print("1.tomar agua")
    print("2.Caminar")
    print("0. Salir")
    print("--> ")
    valor = int(input())
    print("---------------------")
    if valor == 1:
        firulais.set_tomar_aguita()
    elif valor == 2:
        firulais.caminar()
    elif valor != 1 and valor != 2:
        pass
    
    